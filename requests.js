const getPuzzel = (callback) => {
    const request = new XMLHttpRequest();
    request.addEventListener('readystatechange', (e) => {
        if (e.target.readyState === 4 && e.target.status === 200) {
            const data = JSON.parse(e.target.responseText);
            // IF SUCCESSFUL, RETURN FUNCTION WITH 2ND ARGUMENT. 
            callback(undefined, data.puzzle)
        } else if (e.target.readyState === 4) {
            // IF UNSUCCESSFUL, RETURN FUNCTION WITH 1ST ARGUMENT. 
            callback('An error has taken place', undefined)
        }
    });
    request.open('GET', 'http://puzzle.mead.io/puzzle?wordCount=3');
    request.send();

}

// Challenge time!
// 1. Create a new function for getting country details.
// 2. Call the function with two arguments:country code, the callback function.
// 3. Make the HTTP request and call the callback with country information.
// 4. Use the callback to print the country name. 

const getCountry = (countryCode, callback) => {
    const countryRequest = new XMLHttpRequest();
    countryRequest.addEventListener('readystatechange', (e) => {
        if (e.target.readyState === 4 && e.target.status === 200) {
            const countryData = JSON.parse(e.target.responseText);
            const country = countryData.find((country) => country.alpha2Code === countryCode)
            callback(undefined, country)
        } else if (e.target.readyState === 4) {
            callback('An error has happened!');
        }
    });

    countryRequest.open('GET', 'http://restcountries.eu/rest/v2/all');
    countryRequest.send();
}